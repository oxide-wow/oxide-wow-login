# oxide-wow-login
Experimental rust-written World of Warcraft login server

# Features
* login
* respond realmlist
* using mariadb

# Supported versions
* 1.12.1
* 2.0.0 (vanilla)
* 2.0.0 (tbc)
* 2.2.3
* 2.4.3
* maybe more

# REST API
* see [api.yml](docs/api.yml)
* manage users
* login to use for a website

# todo
* cleanup
* increase security
