use byteorder::{LittleEndian, ReadBytesExt};
use std::io::{Cursor, Read};

pub struct ClientMessageReader {
    content: Vec<u8>
}

impl ClientMessageReader {
    pub fn new(content: Vec<u8>) -> Self {
        Self {
            content
        }
    }

    pub fn read_u8(&mut self, byte_index: u64) -> u8 {
        let mut c = Cursor::new(&self.content);
        c.set_position(byte_index);
        c.read_u8().unwrap()
    }

    pub fn read_u16(&mut self, byte_index: u64) -> u16 {
        let mut c = Cursor::new(&self.content);
        c.set_position(byte_index);
        c.read_u16::<LittleEndian>().unwrap()
    }

    pub fn read_vec_u8_20(&mut self, byte_index: u64) -> Vec<u8> {
        let mut c = Cursor::new(&self.content);
        c.set_position(byte_index);
        let mut result: [u8;20] = [0;20];
        c.read(result.as_mut()).unwrap();
        Vec::from(result)
    }

    pub fn read_vec_u8_32(&mut self, byte_index: u64) -> Vec<u8> {
        let mut c = Cursor::new(&self.content);
        c.set_position(byte_index);
        let mut result: [u8;32] = [0;32];
        c.read(result.as_mut()).unwrap();
        Vec::from(result)
    }

    pub fn read_string(&mut self, byte_index: u64) -> String {
        let mut c = Cursor::new(&self.content);
        c.set_position(byte_index);
        let mut result = String::new();
        let _ = c.read_to_string(&mut result);
        result
    }

    pub fn print(&mut self) {
        println!("Message dump: {:?} {}", self.content, hex::encode(&self.content));
    }
}