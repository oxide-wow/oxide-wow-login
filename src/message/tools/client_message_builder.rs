use byteorder::{WriteBytesExt, LittleEndian};
use std::io::{Cursor, Read};

pub struct ClientMessageBuilder {
    signature: u8,
    content: Vec<u8>
}

impl ClientMessageBuilder {
    pub fn new(signature: u8) -> Self {
        Self {
            signature,
            content: vec![]
        }
    }

    pub fn add_u8(&mut self, value: u8) {
        self.content.push(value);
    }

    pub fn add_u16(&mut self, value: u16) {
        self.content.append(Self::u16_to_le(value).as_mut());
    }

    pub fn add_u32(&mut self, value: u32) {
        self.content.append(Self::u32_to_le(value).as_mut());
    }

    pub fn add_vec_u8(&mut self, value: Vec<u8>) {
        let mut v = value;
        self.content.append(v.as_mut());
    }

    pub fn add_string(&mut self, value: String) {
        self.add_vec_u8(value.into_bytes());
    }

    pub fn read_string(&mut self, byte_index: u64) -> String {
        let mut c = Cursor::new(&self.content);
        c.set_position(byte_index);
        let mut result = String::new();
        let _ = c.read_to_string(&mut result);
        result
    }

    pub fn build(&mut self) -> Vec<u8> {
        let mut payload = vec![self.signature];
        payload.append(self.content.as_mut());
        payload
    }

    pub fn u16_to_le(value: u16) -> Vec<u8> {
        let mut append = vec![];
        let _ = append.write_u16::<LittleEndian>(value);
        append
    }

    pub fn u32_to_le(value: u32) -> Vec<u8> {
        let mut append = vec![];
        let _ = append.write_u32::<LittleEndian>(value);
        append
    }
}

#[test]
fn test() {
    let mut builder = ClientMessageBuilder::new(0);
    builder.add_u8(1);
    builder.add_u16(0xaabb);
    builder.add_u32(0xaabbccdd);
    builder.add_vec_u8(vec![2, 4, 6, 8]);
    builder.add_string(String::from("test"));
    builder.add_u8(9);
    assert_eq!(builder.build(), vec![0, 1, 187, 170, 221, 204, 187, 170, 2, 4, 6, 8, 116, 101, 115, 116, 9]);
}