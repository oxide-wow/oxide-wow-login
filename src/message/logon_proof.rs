use crate::message::tools::client_message_reader::ClientMessageReader;
use crate::message::tools::client_message_builder::ClientMessageBuilder;
use crate::message::LogonResult;
use crate::client::client::Client;

pub struct LogonProof {
    pub message_reader: ClientMessageReader
}

// srp crypt message
impl LogonProof {
    pub const SIGNATURE: u8 = 0x01;

    pub fn new(message_reader: ClientMessageReader) -> Self {
        Self {
            message_reader
        }
    }

    pub fn build_response(&mut self, client: &mut Client) -> ClientMessageBuilder {
        match &mut client.srp {
            Some(srp) => {
                let client_ephemeral = self.message_reader.read_vec_u8_32(1);  //SRP A
                let proof = self.message_reader.read_vec_u8_20(33);    //SRP M1
                match srp.verify(client_ephemeral, proof) {
                    Ok(sha) => {    //on success
                        Self::build_auth_success_response(client.client_build, sha)
                    }
                    Err(_) => { //if verify failed
                        println!("proof verify failed");
                        Self::build_auth_fail_response(client.client_build)
                    }
                }
            }
            _ => {  //if no logon challenge happened before
                Self::build_auth_fail_response(client.client_build)
            }
        }
    }

    fn build_auth_response(signature: u8, result: LogonResult) -> ClientMessageBuilder {
        let mut response = ClientMessageBuilder::new(signature);
        response.add_u8(result as u8);
        response
    }

    fn build_auth_fail_response(client_build: u32) -> ClientMessageBuilder {
        let mut response = Self::build_auth_response(Self::SIGNATURE, LogonResult::FailUnknownAccount);
        if client_build > 6005 {
            response.add_u16(0);
        }
        response
    }

    fn build_auth_success_response(client_build: u32, sha: Vec<u8>) -> ClientMessageBuilder {
        let mut response = Self::build_auth_response(Self::SIGNATURE, LogonResult::Success);
        response.add_vec_u8(sha);   //SRP M2

        if client_build < 7359 { //before 2.2.3 (todo: find the exact build number between tbc 2.0.0 and 2.2.3)
            response.add_u32(0);
        } else if client_build < 8606 { //before 2.4.3
            response.add_u32(0);
            response.add_u16(0);
        } else {
            response.add_u32(0x00800000);   //account flag for gm/trial/propass. Always propass
            response.add_u32(0);
            response.add_u16(0);
        }

        response
    }
}