use crate::message::tools::client_message_reader::ClientMessageReader;
use crate::message::tools::client_message_builder::ClientMessageBuilder;
use crate::message::LogonResult;
use crate::client::client::Client;
use crate::crypt::srp::Srp;

// first message from connecting clients
pub struct LogonChallenge {
    pub message_reader: ClientMessageReader
}

impl LogonChallenge {
    pub const SIGNATURE: u8 = 0x00;

    pub fn new(message_reader: ClientMessageReader) -> Self {
        Self {
            message_reader
        }
    }

    pub fn get_client_build(&mut self) -> u32 {
        // let major_version = self.message_reader.read_u8(8);
        // let minor_version = self.message_reader.read_u8(9);
        // let patch_version = self.message_reader.read_u8(10);
        self.message_reader.read_u16(11) as u32
    }

    pub fn get_client_account_name(&mut self) -> String {
        let mut client_account_name = self.message_reader.read_string(34);
        client_account_name.truncate(16);
        println!("Account logging in: {}", client_account_name);
        client_account_name
    }

    pub fn build_response(&mut self, client: &Client) -> ClientMessageBuilder {
        match &client.srp {
            Some(srp) => {
                let mut response = Self::build_auth_response(Self::SIGNATURE, LogonResult::Success);
                response.add_vec_u8(srp.server_ephemeral_public.to_bytes_le());  //SRP B

                response.add_u8(1); //SRP g length
                response.add_u8(Srp::get_g_byte()); //SRP g
                response.add_u8(32);    //SRP N length
                response.add_vec_u8(Srp::get_n_bytes());    //SRP N

                response.add_vec_u8(srp.salt.to_bytes_le()); //SRP s

                response.add_vec_u8(vec![0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);  //?
                response.add_u8(0);

                response
            }
            None => {
                Self::build_auth_response(Self::SIGNATURE, LogonResult::FailUnknownAccount)
            }
        }
    }

    fn build_auth_response(signature: u8, result: LogonResult) -> ClientMessageBuilder {
        let mut response = ClientMessageBuilder::new(signature);
        response.add_u8(0);
        response.add_u8(result as u8);
        response
    }
}