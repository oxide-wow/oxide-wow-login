use crate::message::tools::client_message_reader::ClientMessageReader;
use crate::message::tools::client_message_builder::ClientMessageBuilder;
use crate::client::client::Client;

// requesting realmlist
pub struct RealmList {
    pub message_reader: ClientMessageReader
}

impl RealmList {
    pub const SIGNATURE: u8 = 0x10;

    pub fn new(message_reader: ClientMessageReader) -> Self {
        Self {
            message_reader
        }
    }

    pub fn build_response(&mut self, client: &Client) -> ClientMessageBuilder {
        let mut realms = RealmCollection::new();
        realms.add(Realm::new("oxide-wow".to_string(), "127.0.0.1:8085".to_string()));

        let realms_vec;
        if client.client_build < 7359 { //before 2.2.3 (todo: find the exact build number between tbc 2.0.0 and 2.2.3)
            realms_vec = realms.to_vec_vanilla();
        } else {
            realms_vec = realms.to_vec_tbc();
        }

        let mut response = ClientMessageBuilder::new(Self::SIGNATURE);
        response.add_u16(realms_vec.len() as u16);
        response.add_vec_u8(realms_vec);
        response
    }
}

struct RealmCollection {
    realms: Vec<Realm>
}

impl RealmCollection {
    pub fn new() -> Self {
        Self {
            realms: vec![]
        }
    }

    pub fn add(&mut self, realm: Realm) {
        self.realms.push(realm);
    }

    pub fn to_vec_tbc(&self) -> Vec<u8> {
        let mut result = ClientMessageBuilder::u32_to_le(0);
        result.append(ClientMessageBuilder::u16_to_le(self.realms.len() as u16).as_mut());

        for realm in self.realms.iter() {
            result.append(realm.to_vec().as_mut());
        }

        result.push(RealmList::SIGNATURE);
        result.push(0);
        result
    }

    pub fn to_vec_vanilla(&self) -> Vec<u8> {
        let mut result = ClientMessageBuilder::u32_to_le(0);
        result.push(self.realms.len() as u8);
        result.push(0);
        result.push(0);

        for realm in self.realms.iter() {
            result.append(realm.to_vec().as_mut());
        }

        result.push(2);
        result.push(0);
        result
    }
}

struct Realm {
    name: String,
    address: String
}

impl Realm {
    pub fn new(name: String, address: String) -> Self {
        Self {
            name,
            address
        }
    }

    pub fn to_vec(&self) -> Vec<u8> {
        let mut result = vec![];

        result.push(1); //pvp
        result.push(0); //status
        result.push(0); //color
        result.append(self.name.clone().into_bytes().as_mut());
        result.push(0); //separator
        result.append(self.address.clone().into_bytes().as_mut());
        result.push(0); //separator
        result.append(ClientMessageBuilder::u32_to_le(0).as_mut()); //population level
        result.push(0); //character count
        result.push(1); //timezone
        result.push(0); //?

        result
    }
}

#[test]
fn test() {
    let mut realm_collection = RealmCollection::new();
    realm_collection.add(Realm::new("oxide-wow".to_string(), "127.0.0.1:8085".to_string()));
    assert_eq!(*realm_collection.to_vec_tbc(), vec![0, 0, 0, 0, 1, 0, 1, 0, 0, 111, 120, 105, 100, 101, 45, 119, 111, 119, 0, 49, 50, 55, 46, 48, 46, 48, 46, 49, 58, 56, 48, 56, 53, 0, 0, 0, 0, 0, 0, 1, 0, 16, 0]);
}