pub mod logon_challenge;
pub mod logon_proof;
pub mod realm_list;
pub mod tools;

enum LogonResult {
    Success = 0x00,
    FailBanned = 0x03,          //permanent ban
    FailUnknownAccount = 0x04,
    FailVersionInvalid = 0x09,
    FailSuspended = 0x0c        //temporary ban
}