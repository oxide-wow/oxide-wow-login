use crate::gateway::client_gateway::ClientGateway;
use crate::api::api::Api;
use crate::client::client_manager::ClientManager;
use mysql::Pool;
use crate::client::db::db_adapter::DbAdapter;

mod api;
mod client;
mod crypt;
mod gateway;
mod message;

fn main() {
    let database = Pool::new("mysql://login_user:login_password@127.0.0.1:3306/login").unwrap().get_conn().unwrap();
    let db_adapter = DbAdapter::new(database);
    let client_manager = ClientManager::create(db_adapter);

    let api = Api::new(client_manager.clone());
    api.launch();

    ClientGateway::run(client_manager);
}