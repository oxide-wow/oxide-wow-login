use std::sync::mpsc::{Sender, channel, Receiver};
use async_std::net::{TcpListener, TcpStream};
use futures::StreamExt;
use futures::executor::block_on;
use std::thread::sleep;
use std::time::Duration;
use crate::client::client_manager::ClientManager;
use std::sync::{Arc, Mutex};
use crate::client::client::Client;

static THREAD_NAME: &str = "oxide_login_client_gateway";

pub struct ClientGateway {
    client_receiver: Receiver<TcpStream>,
    client_manager: Arc<Mutex<ClientManager>>
}

impl ClientGateway {
    pub fn run(client_manager: Arc<Mutex<ClientManager>>) {
        let (sender, client_receiver) = channel();

        std::thread::Builder::new().name(String::from(THREAD_NAME)).spawn(move || {
            block_on(Self::listen_incoming(&sender)).unwrap();
        }).unwrap();

        let mut client_gateway = Self {
            client_manager,
            client_receiver
        };
        client_gateway.handle_clients();
    }

    async fn listen_incoming(sender: &Sender<TcpStream>) -> std::io::Result<()> {
        let listener = TcpListener::bind("0.0.0.0:3724").await.unwrap();
        let port = listener.local_addr()?;
        println!("Listening on {}...", port);

        listener.incoming().for_each_concurrent(None, |tcp_stream| async move {
            let tcp_stream = tcp_stream.unwrap();
            let addr = tcp_stream.peer_addr().unwrap();
            println!("Client connected: {:?}", addr);
            sender.send(tcp_stream).unwrap();
        }).await;

        Ok(())
    }

    pub fn handle_clients(&mut self) {
        loop {
            self.client_manager.lock().unwrap().remove_disconnected_clients();

            match self.client_receiver.try_recv() {
                Ok(tcp_stream) => {
                    self.client_manager.lock().unwrap().push_client(Client::new(tcp_stream));
                },
                Err(_) => ()
            }

            self.client_manager.lock().unwrap().handle_all_client_messages();

            sleep(Duration::from_millis(1));
        }
    }
}