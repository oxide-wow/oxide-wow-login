#[derive(Debug, PartialEq, Eq)]
pub struct DbUser {
    pub salt: String,
    pub verifier: String
}

// impl DbUser {
//     pub fn create_from_api_user(api_user: crate::api::model::user::User) -> Self {
//         let salt = crate::api::model::user::User::create_salt();
//         Self {
//             name: api_user.username,
//             salt: salt.to_str_radix(16),
//             verifier: api_user.build_verifier(&salt).to_str_radix(16)
//         }
//     }
// }