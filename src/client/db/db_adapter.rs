use mysql::PooledConn;
use mysql::prelude::Queryable;
use mysql::params;
use num_bigint::BigUint;
use std::ops::Deref;
use crate::api::model::user::User;
use crate::client::db::db_user::DbUser;
use crate::crypt::srp::Srp;

pub struct DbAdapter {
    database: PooledConn
}

impl DbAdapter {
    pub fn new(database: PooledConn) -> Self {
        DbAdapter {
            database
        }
    }

    pub fn create_db_user(&mut self, api_user: User) -> Result<(), ()> {
        let salt = User::create_salt();
        self.database.exec_drop(
            "INSERT INTO `user` (`name`, `salt`, `verifier`) VALUES(:name, :salt, :verifier)",
            params! {
                "name" => &api_user.username,
                "salt" => hex::encode(salt.to_bytes_le()),
                "verifier" => hex::encode(api_user.build_verifier(&salt).to_bytes_le())
            }
        ).map_err(|_| ()).and_then(|_| Ok(()))
    }

    pub fn update_db_user(&mut self, username: String, api_user: User) -> Result<(), ()> {
        let salt = User::create_salt();
        self.database.exec_drop(
            "UPDATE `user` SET `name` = :name, `salt` = :salt, `verifier` = :verifier WHERE `name` = :old_name",
            params! {
                "old_name" => username,
                "name" => &api_user.username,
                "salt" => hex::encode(salt.to_bytes_le()),
                "verifier" => hex::encode(api_user.build_verifier(&salt).to_bytes_le())
            }
        ).map_err(|_| ()).and_then(|_| if self.database.affected_rows() > 0 {
            Ok(())
        } else {
            Err(())
        })
    }

    pub fn delete_db_user(&mut self, username: String) -> Result<(), ()> {
        self.database.exec_drop(
            "DELETE FROM `user` WHERE `name` = :name",
            params! {
                "name" => username
            }
        ).map_err(|_| ()).and_then(|_| if self.database.affected_rows() > 0 {
            Ok(())
        } else {
            Err(())
        })
    }

    pub fn find_db_user(&mut self, username: String) -> Result<DbUser, ()> {
        self.database.exec_map(
            "SELECT `salt`, `verifier` FROM `user` WHERE `name` = :name",
            params! {
                "name" => username
            },
            |(salt, verifier)| {
                DbUser { salt, verifier }
            }
        ).map_err(|_| ()).and_then(|rows| match rows.first() {
            Some(db_user) => {
                Ok(DbUser { salt: db_user.salt.clone(), verifier: db_user.verifier.clone() })
            },
            _ => Err(())
        })
    }

    pub fn check_db_login(&mut self, api_user: User) -> Result<(), ()> {
        match self.find_db_user(api_user.username.clone()) {
            Ok(db_user) => {
                api_user.verify(
                    &BigUint::from_bytes_le(hex::decode(&db_user.salt).unwrap().deref()),
                    &BigUint::from_bytes_le(hex::decode(&db_user.verifier).unwrap().deref()),
                )
            },
            _ => Err(())
        }
    }

    pub fn create_srp(&mut self, username: String) -> Option<Srp> {
        match self.find_db_user(username.clone()) {
            Ok(db_user) => {
                Some(Srp::new(
                    username.into_bytes(),
                    BigUint::from_bytes_le(hex::decode(&db_user.salt).unwrap().deref()),
                    BigUint::from_bytes_le(hex::decode(&db_user.verifier).unwrap().deref())
                ))
            },
            _ => None
        }
    }
}