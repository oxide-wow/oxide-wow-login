use async_std::net::{TcpStream, SocketAddr};
use async_std::future::timeout;
use futures::{AsyncReadExt, AsyncWriteExt};
use std::time::Duration;
use crate::crypt::srp::Srp;
use crate::message::tools::client_message_reader::ClientMessageReader;
use crate::message::tools::client_message_builder::ClientMessageBuilder;
use crate::message::logon_challenge::LogonChallenge;
use crate::message::logon_proof::LogonProof;
use crate::message::realm_list::RealmList;
use crate::client::db::db_adapter::DbAdapter;

pub struct Client {
    pub addr: SocketAddr,
    pub tcp_stream: TcpStream,
    pub connected: bool,
    pub srp: Option<Srp>,
    pub client_build: u32,
    pub client_account_name: String
}

impl Client {
    pub fn new(tcp_stream: TcpStream) -> Self {
        Self {
            addr: tcp_stream.peer_addr().unwrap(),
            tcp_stream,
            connected: true,
            srp: Option::None,
            client_build: 0,
            client_account_name: String::new()
        }
    }

    pub async fn handle_messages(&mut self, db_adapter: &mut DbAdapter) {
        let mut buffer = vec![0; 2800];
        match timeout(Duration::from_millis(1), self.tcp_stream.read(&mut buffer)).await {
            Ok(s) => {
                if s.unwrap() > 0 {
                    if let Some(end) = buffer.iter().rposition(|x| *x != 0) {
                        // println!("cutting {} zeros from client buffer", buffer.len() - end - 1);
                        buffer.truncate(end + 1);
                    }
                    self.on_package_receive(db_adapter, ClientMessageReader::new(buffer)).await;
                } else {
                    println!("Client disconnected: {:?}", self.addr);
                    self.connected = false;
                }
            }
            _ => {}
        }
    }

    async fn on_package_receive(&mut self, db_adapter: &mut DbAdapter, mut message_reader: ClientMessageReader) {
        match message_reader.read_u8(0) {
            LogonChallenge::SIGNATURE => {
                let mut message = LogonChallenge::new(message_reader);
                self.client_build = message.get_client_build();
                self.client_account_name = message.get_client_account_name();
                self.srp = db_adapter.create_srp(self.client_account_name.clone());
                self.send_package(message.build_response(self)).await;
            }
            LogonProof::SIGNATURE => {
                let mut message = LogonProof::new(message_reader);
                let response = message.build_response(self);
                self.send_package(response).await;
            }
            RealmList::SIGNATURE => {
                let mut message = RealmList::new(message_reader);
                self.send_package(message.build_response(self)).await;
            }
            _ => {
                println!("unknown package");
                message_reader.print();
            }
        };
    }

    async fn send_package(&mut self, mut message_builder: ClientMessageBuilder) {
        let data = message_builder.build();
        // println!("Response: {:?}", data);
        let _ = self.tcp_stream.write(data.as_ref()).await;
    }
}