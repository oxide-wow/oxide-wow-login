use futures::executor::block_on;
use crate::client::client::Client;
use std::sync::{Arc, Mutex};
use crate::api::model::api_session::ApiSession;
use crate::client::db::db_adapter::DbAdapter;

pub struct ClientManager {
    clients: Vec<Client>,
    pub db_adapter: DbAdapter
}

impl ClientManager {
    pub fn create(db_adapter: DbAdapter) -> Arc<Mutex<Self>> {
        Arc::new(Mutex::new(Self {
            clients: vec![],
            db_adapter
        }))
    }

    pub fn push_client(&mut self, client: Client) {
        self.clients.push(client);
    }

    pub fn remove_disconnected_clients(&mut self) {
        self.clients.retain(|client| {
            client.connected
        });
    }

    pub fn handle_all_client_messages(&mut self) {
        for client in &mut self.clients {
            block_on(client.handle_messages(&mut self.db_adapter));
        }
    }

    pub fn build_client_api_session(&mut self, username: String) -> Result<ApiSession, ()> {
        for client in &mut self.clients {
            if client.client_account_name == username && client.srp.is_some() {
                let srp = client.srp.as_ref().unwrap();
                if srp.session_key.is_some() {
                    return Ok(ApiSession {
                        salt: srp.salt.to_str_radix(16),
                        verifier: srp.verifier.to_str_radix(16),
                        session_key: srp.session_key.as_ref().unwrap().to_str_radix(16)
                    });
                }
            }
        }
        Err(())
    }
}