extern crate serde;

use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
pub struct ApiSession {
    pub salt: String,
    pub verifier: String,
    pub session_key: String
}