extern crate serde;

use serde::{Deserialize, Serialize};
use rand::rngs::OsRng;
use num_bigint::BigUint;
use rand::RngCore;
use sha1::{Sha1, Digest};
use crate::crypt::srp::{N, G, Srp};

#[derive(Deserialize, Serialize)]
pub struct User {
    pub username: String,
    pub auth_hash: String
}

impl User {
    pub fn create_salt() -> BigUint {
        let mut salt = [0u8; 32];
        OsRng.fill_bytes(&mut salt);
        BigUint::from_bytes_le(&salt)
    }

    pub fn build_verifier(self, salt: &BigUint) -> BigUint {
        let x = {
            let mut hasher = Sha1::new();
            Digest::update(&mut hasher,salt.to_bytes_le());
            Digest::update(&mut hasher,hex::decode(self.auth_hash).unwrap());
            BigUint::from_bytes_le(hasher.finalize().as_ref())
        };

        Srp::powm(&G, &x, &N)
    }

    pub fn verify(self, salt: &BigUint, verifier: &BigUint) -> Result<(), ()> {
        if self.build_verifier(salt).eq(verifier) {
            Ok(())
        } else {
            Err(())
        }
    }
}

#[test]
fn test() {
    let user = User {
        username: "PLAYER".to_string(),
        auth_hash: "3ce8a96d17c5ae88a30681024e86279f1a38c041".to_string()
    };
    let salt = BigUint::parse_bytes(b"81d8621314451dc5f276badafbd8381c6b336da0eb7fca61809bd894f13aa2eb", 16).unwrap();
    let verifier = user.build_verifier(&salt);
    assert_eq!(hex::encode(verifier.to_bytes_be()), "011aa4d7860b5efe325200b365a76d8d4f80f2883ae4bbbdfc50ddc0562ecb3e");
}