use warp::{Filter, Reply};
use warp::reply::Response;
use crate::client::client_manager::ClientManager;
use std::sync::{Mutex, Arc};
use warp::http::StatusCode;
use crate::api::model::user::User;

pub static THREAD_NAME: &str = "oxide_login_api";

pub struct Api {
    client_manager: Arc<Mutex<ClientManager>>
}

impl Api {
    pub fn new(client_manager: Arc<Mutex<ClientManager>>) -> Self {
        Self {
            client_manager
        }
    }

    pub fn launch(self) {
        std::thread::Builder::new().name(String::from(THREAD_NAME)).spawn(move || {
            let rt = tokio::runtime::Runtime::new().unwrap();
            rt.block_on(self.listen());
        }).unwrap();
    }

    async fn listen(self) {
        let api = Arc::new(Mutex::new(self));
        let api_filter = warp::any().map(move || api.clone());

        let get_health = warp::path("health")
            .map(|| "I'm alive");

        let get_session = warp::path("session")
            .and(api_filter.clone())
            .and(warp::path::param())
            .map(|api: Arc<Mutex<Self>>, session_id: String| api.lock().unwrap().get_session(session_id));

        let get = warp::get().and(
            get_health
                .or(get_session)
        );


        let post_login = warp::path("login")
            .and(api_filter.clone())
            .and(warp::body::json())
            .map(|api: Arc<Mutex<Self>>, user: User| api.lock().unwrap().post_login(user));

        let post_user = warp::path("user")
                .and(api_filter.clone())
                .and(warp::body::json())
                .map(|api: Arc<Mutex<Self>>, user: User| api.lock().unwrap().post_user(user));

        let post = warp::post().and(
            post_login
                .or(post_user)
        );


        let put_user = warp::path("user")
            .and(api_filter.clone())
            .and(warp::path::param())
            .and(warp::body::json())
            .map(|api: Arc<Mutex<Self>>, username: String, user: User| api.lock().unwrap().put_user(username, user));

        let put = warp::put().and(
            put_user
        );


        let delete_user = warp::path("user")
            .and(api_filter.clone())
            .and(warp::path::param())
            .map(|api: Arc<Mutex<Self>>, username: String| api.lock().unwrap().delete_user(username));

        let delete = warp::delete().and(
            delete_user
        );


        let routes = get.or(post).or(put).or(delete);
        warp::serve(routes).run(([0, 0, 0, 0], 8001)).await;
    }

    pub fn post_login(&self, user: User) -> Response {
        match self.client_manager.lock().unwrap().db_adapter.check_db_login(user) {
            Ok(()) => warp::reply().into_response(),
            _ => warp::reply::with_status(warp::reply(), StatusCode::UNAUTHORIZED).into_response()
        }
    }

    pub fn get_session(&self, username: String) -> Response {
        match self.client_manager.lock().unwrap().build_client_api_session(username) {
            Ok(api_session) => warp::reply::json(&api_session).into_response(),
            _ => warp::reply::with_status(warp::reply(), StatusCode::NOT_FOUND).into_response()
        }
    }

    pub fn post_user(&self, user: User) -> Response {
        match self.client_manager.lock().unwrap().db_adapter.create_db_user(user) {
            Ok(()) => warp::reply().into_response(),
            _ => warp::reply::with_status(warp::reply(), StatusCode::CONFLICT).into_response()
        }
    }

    pub fn put_user(&self, username: String, user: User) -> Response {
        match self.client_manager.lock().unwrap().db_adapter.update_db_user(username, user) {
            Ok(()) => warp::reply().into_response(),
            _ => warp::reply::with_status(warp::reply(), StatusCode::NOT_FOUND).into_response()
        }
    }

    pub fn delete_user(&self, username: String) -> Response {
        match self.client_manager.lock().unwrap().db_adapter.delete_db_user(username) {
            Ok(()) => warp::reply().into_response(),
            _ => warp::reply::with_status(warp::reply(), StatusCode::NOT_FOUND).into_response()
        }
    }
}