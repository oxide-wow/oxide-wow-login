use lazy_static::lazy_static;
use rand::rngs::OsRng;
use rand::RngCore;
use sha1::{Sha1, Digest};
use num_bigint::BigUint;

lazy_static! {
    pub static ref N: BigUint = BigUint::parse_bytes(b"894b645e89e1535bbdad5b8b290650530801b18ebfbf5e8fab3c82872a3e9bb7", 16).unwrap();
    pub static ref G: BigUint = BigUint::from_bytes_le(&[7]);
}

pub struct Srp {
    username: Vec<u8>,
    pub salt: BigUint,
    pub verifier: BigUint,
    server_ephemeral_private: BigUint,
    pub server_ephemeral_public: BigUint,
    pub session_key: Option<BigUint>
}

impl Srp {
    pub fn new(username: Vec<u8>, salt: BigUint, verifier: BigUint) -> Self {
        let server_ephemeral_private = Self::generate_server_ephemeral_private();
        let server_ephemeral_public = Self::build_server_ephemeral_public(&verifier, &server_ephemeral_private);

        Self {
            username,
            salt,
            verifier,
            server_ephemeral_private,
            server_ephemeral_public,
            session_key: None
        }
    }

    pub fn verify(&mut self, client_ephemeral: Vec<u8>, proof: Vec<u8>) -> Result<Vec<u8>, ()> {
        if client_ephemeral.is_empty() {
            return Err(());
        }
        let a = BigUint::from_bytes_le(client_ephemeral.as_ref());
        let vk = self.build_vk(&client_ephemeral);

        let k = Self::build_k(vk);
        let m = self.build_m(&a, &k);
        if proof != m.to_bytes_le() {
            return Err(());
        }

        let result = self.build_result_hash(&client_ephemeral, &m, &k);
        self.session_key = Some(k);
        Ok(result)
    }

    pub fn build_vk(&self, client_ephemeral: &Vec<u8>) -> BigUint {
        let mut hasher = Sha1::new();
        Digest::update(&mut hasher,client_ephemeral);
        Digest::update(&mut hasher, self.server_ephemeral_public.to_bytes_le());
        let sha = hasher.finalize();
        let u = BigUint::from_bytes_le(sha.as_ref());
        let k = BigUint::from_bytes_le(client_ephemeral) * Self::powm(&self.verifier, &u, &N);
        Self::powm(&k, &self.server_ephemeral_private, &N)
    }

    pub fn build_result_hash(&self, client_ephemeral: &Vec<u8>, m: &BigUint, k: &BigUint) -> Vec<u8> {
        let a = BigUint::from_bytes_le(client_ephemeral.as_ref());
        let mut hasher = Sha1::new();
        Digest::update(&mut hasher,a.to_bytes_le());
        Digest::update(&mut hasher,m.to_bytes_le());
        Digest::update(&mut hasher,k.to_bytes_le());
        hasher.finalize().to_vec()
    }

    pub fn build_t3_hash(&self) -> Vec<u8> {
        let mut hasher = Sha1::new();
        Digest::update(&mut hasher,N.to_bytes_le());
        let mut hash = hasher.finalize();
        let mut hasher = Sha1::new();
        Digest::update(&mut hasher,G.to_bytes_le());
        let sha = hasher.finalize();
        for i in 0..20 {
            hash[i] ^= sha[i];
        }
        hash.to_vec()
    }

    pub fn build_username_hash(&self) -> Vec<u8> {
        let mut hasher = Sha1::new();
        Digest::update(&mut hasher,&self.username);
        hasher.finalize().to_vec()
    }

    pub fn build_m(&self, a: &BigUint, k: &BigUint) -> BigUint {
        let mut hasher = Sha1::new();
        Digest::update(&mut hasher,self.build_t3_hash());
        Digest::update(&mut hasher,self.build_username_hash());
        Digest::update(&mut hasher,self.salt.to_bytes_le());
        Digest::update(&mut hasher,&a.to_bytes_le());
        Digest::update(&mut hasher,self.server_ephemeral_public.to_bytes_le());
        Digest::update(&mut hasher,k.to_bytes_le());
        let sha = hasher.finalize();
        BigUint::from_bytes_le(sha.as_ref())
    }

    pub fn build_k(session_key: BigUint) -> BigUint {
        let session_key_vec = session_key.to_bytes_le();
        let mut t1 = [0u8;16];
        for i in 0..16 {
            t1[i] = session_key_vec[i * 2];
        }
        let mut hasher = Sha1::new();
        Digest::update(&mut hasher,&t1);
        let sha = hasher.finalize();
        let mut v_k = [0u8;40];
        for i in 0..20 {
            v_k[i * 2] = sha[i];
        }
        for i in 0..16 {
            t1[i] = session_key_vec[i * 2 + 1];
        }
        let mut hasher = Sha1::new();
        Digest::update(&mut hasher,&t1);
        let sha = hasher.finalize();
        for i in 0..20 {
            v_k[i * 2 + 1] = sha[i];
        }
        BigUint::from_bytes_le(v_k.as_ref())
    }

    pub fn build_verifier(username: &Vec<u8>, password: &Vec<u8>, salt: &BigUint) -> Vec<u8> {
        let p = {
            let mut hasher = Sha1::new();
            Digest::update(&mut hasher,username);
            Digest::update(&mut hasher,b":");
            Digest::update(&mut hasher,password);
            hasher.finalize()
        };

        let x = {
            let mut hasher = Sha1::new();
            Digest::update(&mut hasher,salt.to_bytes_le());
            Digest::update(&mut hasher,p);
            BigUint::from_bytes_le(hasher.finalize().as_ref())
        };

        Self::powm(&G, &x, &N).to_bytes_le()
    }

    pub fn generate_server_ephemeral_private() -> BigUint {
        let mut b = [0u8; 19];
        OsRng.fill_bytes(&mut b);
        BigUint::from_bytes_le(&b)
    }

    pub fn build_server_ephemeral_public(verifier: &BigUint, b: &BigUint) -> BigUint {
        let gmod = Self::powm(&G, &b, &N);
        ((verifier * BigUint::from(3 as u8)) + gmod) % &N.clone()
    }

    pub fn get_g_byte() -> u8 {
        let g = G.to_bytes_le();
        *g.get(0).unwrap()
    }

    pub fn get_n_bytes() -> Vec<u8> {
        N.to_bytes_le()
    }

    //this function is from crate "srp". Sadly not pub so I had to copy&paste...
    pub fn powm(base: &BigUint, exp: &BigUint, modulus: &BigUint) -> BigUint {
        let zero = BigUint::from(0u32);
        let one = BigUint::from(1u32);
        let two = BigUint::from(2u32);
        let mut exp = exp.clone();
        let mut result = one.clone();
        let mut base = base % modulus;

        while exp > zero {
            if &exp % &two == one {
                result = (result * &base) % modulus;
            }
            exp >>= 1;
            base = (&base * &base) % modulus;
        }
        result
    }
}

#[test]
fn test() {
    let username = Vec::from("PLAYER");
    let password = Vec::from("PLAYER");
    let salt = BigUint::parse_bytes(b"81d8621314451dc5f276badafbd8381c6b336da0eb7fca61809bd894f13aa2eb", 16).unwrap();
    let verifier= BigUint::from_bytes_le(Srp::build_verifier(&username, &password, &salt).as_ref());
    assert_eq!(hex::encode(verifier.to_bytes_be()), "011aa4d7860b5efe325200b365a76d8d4f80f2883ae4bbbdfc50ddc0562ecb3e");

    let server_ephemeral_private = BigUint::parse_bytes(b"5e7e56b8c6d9e741f68d17b69022ee3b2a305e", 16).unwrap();
    let server_ephemeral_public = Srp::build_server_ephemeral_public(&verifier, &server_ephemeral_private);
    assert_eq!(hex::encode(server_ephemeral_public.to_bytes_be()), "1a9b84b4463c18e76eb0575ae483feaee94480d7ed616f4b2e70fe49234b3bb2");

    let mut srp = Srp {
        username,
        salt,
        verifier,
        server_ephemeral_private,
        server_ephemeral_public,
        session_key: None
    };
    let a = BigUint::parse_bytes(b"b3a3fbe00ed9c195ee6c704c8c30e84b89de80d76aab437290f6faca2a234a16", 16).unwrap();
    let client_ephemeral = a.to_bytes_le();
    let session_key = srp.build_vk(&client_ephemeral);
    assert_eq!(hex::encode(session_key.to_bytes_be()), "3f6549acb0437c0b2f174b7554d10313220ac8b2749a335281db23a84a6b5f71");

    let k = Srp::build_k(session_key);
    assert_eq!(hex::encode(k.to_bytes_be()), "1d50fb7fd1a70fbdddbd7e9c87709937a11cb2e51d5ecce8369219e8f793069a91fae1bd08c65c2f");

    let m = srp.build_m(&a, &k);
    assert_eq!(hex::encode(m.to_bytes_be()), "64bf5e7da1f503b6bdb65377f04e46776614ef78");

    let result = srp.build_result_hash(&client_ephemeral, &m, &k);
    assert_eq!(hex::encode(result), "22c3c281599d137ba1a055444f1b010115aa1a2e");

    let proof = BigUint::parse_bytes(b"64bf5e7da1f503b6bdb65377f04e46776614ef78", 16).unwrap().to_bytes_le();
    assert_eq!(hex::encode(srp.verify(client_ephemeral, proof).unwrap()), "22c3c281599d137ba1a055444f1b010115aa1a2e");
}