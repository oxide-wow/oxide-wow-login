CREATE TABLE `user` (
	`name` VARCHAR(64) NULL,
	`salt` VARCHAR(64) NULL,
	`verifier` VARCHAR(64) NULL,
	`created` DATETIME NULL DEFAULT current_timestamp(),
	PRIMARY KEY (`name`)
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB;